package com.tasj;

import com.codeborne.selenide.CollectionCondition;

import java.util.List;

/**
 * Created by asevruk on 4/13/2016.
 */
public class CustomConditions {
    public static CollectionCondition exactTexts(List<String> expectedText){
        String[] texts = expectedText.toArray(new String[0]);
        return CollectionCondition.exactTexts(texts);
    }
}
