package tasj;

import cucumber.api.java.After;

import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created by asevruk on 4/13/2016.
 */
public class ToDosStepHooks {

    @After("@clean")
    public  void clearData(){
        executeJavaScript("localStorage.clear()");
        open("http://todomvc.com");
    }
}
