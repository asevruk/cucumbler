package tasj;



import com.codeborne.selenide.ElementsCollection;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.*;
import static com.tasj.CustomConditions.exactTexts;

/**
 * Created by asevruk on 4/13/2016.
 */
public class ToDosStepdefs {
    ElementsCollection tasks=$$("#todo-list>li");

    @Given("^open ToDoMVC page$")
    public void open_ToDoMVC_page()  {
       open("https://todomvc4tasj.herokuapp.com/");
    }

    @When("^add tasks (.*)$")
    public void add_tasks(List<String> taskTexts) {
        for (String text:taskTexts) {
            $("#new-todo").setValue(text).pressEnter();
            
        }
    }

    @Then("^tasks in the list (.*)$")
    public void tasks_in_the_list(List<String> taskText){

       tasks.shouldHave(exactTexts(taskText));
    }


    @And("^delete '(.*)'$")
    public void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().find(".destroy").click();
    }


}
