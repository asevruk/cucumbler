Feature: ToDos Operations
  Background:
    Given open ToDoMVC page
@clean
    Scenario: add task
    When add tasks a, b, c
    Then tasks in the list a, b, c

@clean
    Scenario: delete task
    When  add tasks a, b
    And delete 'a'
    Then  tasks in the list b
